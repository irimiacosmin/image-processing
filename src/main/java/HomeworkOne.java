import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.util.*;
import java.util.function.Function;

public class HomeworkOne {

    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    private enum TestType {
        TYPE_ONE(Imgproc.COLOR_BGR2RGB), TYPE_TWO(Imgproc.COLOR_BGR2RGB), TYPE_THREE(Imgproc.COLOR_BGR2HSV),
        TYPE_FOUR(Imgproc.COLOR_BGR2HSV), TYPE_FIVE(Imgproc.COLOR_BGR2HSV), TYPE_SIX(Imgproc.COLOR_BGR2YCrCb),
        TYPE_SEVEN(Imgproc.COLOR_BGR2YCrCb);

        private int color;
    }

    private final static Map<TestType, Function<double[], Boolean>> testTypeFunctionMap =
            new HashMap<TestType, Function<double[], Boolean>>() {
                {
                    put(TestType.TYPE_ONE, (pixels) -> {
                        double r = pixels[0], g = pixels[1], b = pixels[2];
                        return r > 95 && g > 40 && b > 20
                                && ((int) PixelUtils.max(r, g, b) - (int) PixelUtils.min(r, g, b)) > 15
                                && Math.abs((int) r - (int) g) > 15 && r > g && r > b;
                    });
                    put(TestType.TYPE_TWO, (pixels) -> {
                        double r = pixels[0], g = pixels[1], b = pixels[2];
                        double rped = g != 0 ? r / g : (int) r / 0.001;
                        double num = Math.pow(r + g + b, 2);
                        int rb = (int) r * (int) b;
                        int rg = (int) r * (int) g;
                        return rped > 1.185 && (rb / num) > 0.107 && (rg / num) > 0.112;
                    });
                    put(TestType.TYPE_THREE, (pixels) -> {
                        double h = pixels[0], s = pixels[1], v = pixels[2];
                        return v >= 0.4 * 255 && 0.2 * 255 < s && s < 0.6 * 255 && (h < 12.5 || h > 167.5);
                    });
                    put(TestType.TYPE_FOUR, (pixels) -> {
                        double h = pixels[0], s = pixels[1], v = pixels[2];
                        return 0 <= h && h <= 25 && (0.23 * 255) <= s
                                && s <= (0.68 * 255) && (0.35 * 255) <= v && v <= 255;
                    });
                    put(TestType.TYPE_FIVE, (pixels) -> {
                        double h = pixels[0], s = pixels[1], v = pixels[2];
                        return (h <= 25 || h >= 170) && (0.2 * 255) <= s && s <= 255 && (0.35 * 255) <= v && v <= 255;
                    });
                    put(TestType.TYPE_SIX, (pixels) -> {
                        double y = pixels[0], cr = pixels[1], cb = pixels[2];
                        return (y > 80) && 85 < cb && cb < 135 && 135 < cr && cr < 180;
                    });
                    put(TestType.TYPE_SEVEN, (pixels) -> {
                        double y = pixels[0], cr = pixels[1], cb = pixels[2];
                        return 77 < cb && cb < 127 && 133 < cr && cr < 173;
                    });
                }
            };

    private Mat detectSkin(Mat inputImage, TestType testType, Integer color) {
        Mat returnImage = inputImage.clone();
        int height = inputImage.rows();
        int width = inputImage.cols();
        int channels = inputImage.channels();

        Imgproc.cvtColor(inputImage, returnImage, color);

        for (int row = 0; row < height; row++) {
            for (int col = 0; col < width; col++) {
                double[] data = returnImage.get(row, col);

                int newValue = testTypeFunctionMap.get(testType).apply(data) ? 255 : 0;
                for (int ch = 0; ch < channels; ch++) {
                    data[ch] = newValue;
                }
                returnImage.put(row, col, data);
            }
        }
        return returnImage;
    }

    private int findGreatestContourIndex(List<MatOfPoint> contours) {
        double largestArea = 0;
        int largestContourIndex = -1, i = 0, totalContours = contours.size();
        while (i < totalContours) {
            double area = Imgproc.contourArea(contours.get(i));
            if (area > largestArea) {
                largestArea = area;
                largestContourIndex = i;
            }
            i++;
        }
        return largestContourIndex;
    }

    private Mat getImageWithContourAroundTheFace(Mat inputImage) {
        TestType typeOne = TestType.TYPE_ONE;
        Mat testOneResult = detectSkin(inputImage, typeOne, typeOne.getColor());

        Mat testOneGrayscale = testOneResult.clone();
        Imgproc.cvtColor(testOneResult, testOneGrayscale, Imgproc.COLOR_BGR2GRAY);
        List<MatOfPoint> contours = new ArrayList<>();
        Mat hierarchy = new Mat();
        Imgproc.findContours(testOneGrayscale, contours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);

        int largestContourIndex = findGreatestContourIndex(contours);
        MatOfPoint greatestContour = contours.get(largestContourIndex);
        Rect rect = Imgproc.boundingRect(greatestContour);
        Imgproc.rectangle(inputImage,
                new Point(rect.x, rect.y),
                new Point(rect.x + rect.width, rect.y + rect.height),
                new Scalar(255, 0, 0), 2);
        Imgproc.drawContours(inputImage, Collections.singletonList(greatestContour), -1, new Scalar(0, 0, 255), 3);

        return inputImage;
    }

    private Mat getEmoticonImage() {
        Mat aux = Mat.zeros(512, 512, CvType.CV_32F);
        Mat emojiMatrix = aux.clone();
        Imgproc.cvtColor(aux, emojiMatrix, Imgproc.COLOR_GRAY2BGR);


        Imgproc.circle(emojiMatrix, new Point(255, 220), 120, new Scalar(0, 255, 255), -1);
        Imgproc.circle(emojiMatrix, new Point(200, 200), 15, new Scalar(0, 0, 0), -1);
        Imgproc.circle(emojiMatrix, new Point(310, 200), 15, new Scalar(0, 0, 0), -1);
        Imgproc.line(emojiMatrix, new Point(230, 270), new Point(280, 270), new Scalar(0, 0, 255), 5);

        Imgproc.ellipse(emojiMatrix, new Point(255, 255), new Size(70, 40), 0, 0, 180, new Scalar(74, 74, 255), -1);
        Imgproc.ellipse(emojiMatrix, new Point(255, 255), new Size(70, 15), 0, 0, 180, new Scalar(0, 255, 255), -1);
        Imgproc.ellipse(emojiMatrix, new Point(280, 270), new Size(50, 15), 260, 90, 270, new Scalar(7, 7, 213), -1);
        return emojiMatrix;
    }

    public void solve() {
        String IMAGE_NAME = "face.jpg";
        Mat image = Imgcodecs.imread(Main.IMPORT_DIRECTORY + IMAGE_NAME);
        System.out.println(image.rows() + " " + image.cols());

        Arrays.stream(TestType.values()).forEach((testType -> {
            Mat resultImage = detectSkin(image, testType, testType.getColor());
            Imgcodecs.imwrite(Main.getFileNameWithSuffix(Main.HOMEWORK_ONE_DIRECTORY, IMAGE_NAME, testType.toString()), resultImage);
        }));

        Imgcodecs.imwrite(Main.getFileNameWithSuffix(Main.HOMEWORK_ONE_DIRECTORY, IMAGE_NAME, "Contoured"), getImageWithContourAroundTheFace(image));
        Imgcodecs.imwrite(Main.getFileNameWithSuffix(Main.HOMEWORK_ONE_DIRECTORY,"Emoji.jpg", ""), getEmoticonImage());
    }
}
