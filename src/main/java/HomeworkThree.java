import homeworkThree.Limitation;
import homeworkThree.OCRFile;
import homeworkThree.PreprocessingTechnique;
import net.sourceforge.tess4j.Tesseract;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

public class HomeworkThree {

    private final Tesseract tesseract;
    private static final String STRING_EMPTY = "";

    private final Map<OCRFile, Map<Limitation, Map<PreprocessingTechnique, String>>> ocrResults = new HashMap<>();

    public HomeworkThree() {
        tesseract = new Tesseract();
        tesseract.setDatapath(Main.TESSDATA_DIRECTORY);
        initMaps();
    }

    private void initMaps() {
        Arrays.asList(OCRFile.values()).forEach(ocrFile -> {
            ocrResults.put(ocrFile, new HashMap<>());
            Arrays.asList(Limitation.values()).forEach(limitation -> {
                ocrResults.get(ocrFile).put(limitation, new HashMap<>());
                Arrays.asList(PreprocessingTechnique.values()).forEach(preprocessingTechnique -> {
                    ocrResults.get(ocrFile).get(limitation).put(preprocessingTechnique, STRING_EMPTY);
                });
            });
        });
    }

    private void addOcrResult(OCRFile ocrFile, Limitation limitation,
                              PreprocessingTechnique preprocessingTechnique, String text) {
        ocrResults.get(ocrFile).get(limitation).put(preprocessingTechnique, text);
    }

    private String getTextFromImage(Mat image, String fileName, String suffix) {
        String savedFilePath = Main.getFileNameWithSuffix(Main.HOMEWORK_THREE_DIRECTORY, fileName, suffix);
        Imgcodecs.imwrite(savedFilePath, image);
        return getTextFromImage(savedFilePath);
    }

    private String getTextFromImage(String path) {
        try {
            File file = new File(path);
            return tesseract.doOCR(file);
        } catch (Exception e) {
            System.err.println(Arrays.toString(e.getStackTrace()));
            return STRING_EMPTY;
        }
    }

    public void solve() {
        Arrays.asList(OCRFile.values()).forEach(ocrFile -> {
            String filePath = Main.IMPORT_DIRECTORY + "lab3/" + ocrFile.getFileName();
            Mat originalImage = Imgcodecs.imread(filePath);

            Arrays.asList(Limitation.values()).forEach(limitation -> {
                Mat limitationImage = limitation.getMethod().apply(originalImage.clone());
                Arrays.asList(PreprocessingTechnique.values()).forEach(preprocessingTechnique -> {
                    Mat preprocessedImage = preprocessingTechnique.getMethod().apply(limitationImage.clone());
                    String operationsApplied = limitation.toString() + "_" + preprocessingTechnique.toString();
                    String textFromImage = getTextFromImage(preprocessedImage, ocrFile.getFileName(), operationsApplied);
                    addOcrResult(ocrFile, limitation, preprocessingTechnique, textFromImage);
                });
            });
        });

        analyzeData();
    }

    private void analyzeData() {
        Arrays.asList(OCRFile.values()).forEach(ocrFile -> {
            System.out.println("\n\nFile: " + ocrFile);
            String originalText = ocrResults.get(ocrFile).get(Limitation.NONE).get(PreprocessingTechnique.NONE);
            Arrays.asList(Limitation.values()).forEach(limitation -> {
                AtomicReference<Double> initialValue = new AtomicReference<>(0.0);
                Arrays.asList(PreprocessingTechnique.values()).forEach(preprocessingTechnique -> {
                    String extractedText = ocrResults.get(ocrFile).get(limitation).get(preprocessingTechnique);
                    String suffix = "";
                    Double compare = StringUtils.compare(originalText, extractedText);
                    if (preprocessingTechnique == PreprocessingTechnique.NONE) {
                        initialValue.set(compare);
                    } else {
                        if (initialValue.get() < compare) {
                            suffix = " -> BETTER THAN ORIGINAL";
                        }
                    }
                    System.out.println(limitation + " " + preprocessingTechnique + " " + compare + suffix);
                });
            });
        });
    }
}