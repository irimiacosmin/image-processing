import jdk.nashorn.internal.ir.annotations.Ignore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public class HomeworkTwo {

    @AllArgsConstructor
    @Getter
    private enum TransformationType {
        PIXEL_AVERAGE,
        PIXEL_WEIGHTED_AVERAGE,
        DESATURATION,
        DECOMPOSITION_MAX,
        DECOMPOSITION_MIN,
        SINGLE_COLOR_CHANNEL_RED,
        SINGLE_COLOR_CHANNEL_GREEN,
        SINGLE_COLOR_CHANNEL_BLUE,
        PARTITION_SHADE(Imgproc.COLOR_RGB2GRAY);

        TransformationType() {
            this.color = Imgproc.COLOR_BGR2RGB;
        }

        private final int color;
    }

    private final static Map<TransformationType, Function<double[], Double>> transformationTypeFunctionMap =
            new HashMap<TransformationType, Function<double[], Double>>() {
                {
                    put(TransformationType.PIXEL_AVERAGE, (pixels) -> {
                        double r = pixels[0], g = pixels[1], b = pixels[2];
                        return (double) (((int) r + (int) g + (int) b) / 3);
                    });
                    put(TransformationType.PIXEL_WEIGHTED_AVERAGE, (pixels) -> {
                        double r = pixels[0], g = pixels[1], b = pixels[2];
                        return (r * 0.3 + g * 0.59 + b * 0.11);
                    });
                    put(TransformationType.DESATURATION, (pixels) -> {
                        double r = pixels[0], g = pixels[1], b = pixels[2];
                        double minVal = PixelUtils.min(r, g, b);
                        double maxVal = PixelUtils.max(r, g, b);
                        return (double) (((int) minVal + (int) maxVal) / 2);
                    });
                    put(TransformationType.DECOMPOSITION_MAX, (pixels) -> {
                        double r = pixels[0], g = pixels[1], b = pixels[2];
                        return PixelUtils.max(r, g, b);
                    });
                    put(TransformationType.DECOMPOSITION_MIN, (pixels) -> {
                        double r = pixels[0], g = pixels[1], b = pixels[2];
                        return PixelUtils.min(r, g, b);
                    });
                    put(TransformationType.SINGLE_COLOR_CHANNEL_RED, (pixels) -> pixels[0]);
                    put(TransformationType.SINGLE_COLOR_CHANNEL_GREEN, (pixels) -> pixels[1]);
                    put(TransformationType.SINGLE_COLOR_CHANNEL_BLUE, (pixels) -> pixels[2]);
                    put(TransformationType.PARTITION_SHADE, (pixels) -> {
                        double shadesPerPartitions = Math.floor(255 / 24);
                        return pixels[0] / shadesPerPartitions * shadesPerPartitions - Math.floor(shadesPerPartitions / 2);
                    });
                }
            };

    private Mat imageToGrayscale(Mat inputImage, TransformationType testType, Integer color) {
        Mat returnImage = inputImage.clone();
        int height = inputImage.rows();
        int width = inputImage.cols();
        Imgproc.cvtColor(inputImage, returnImage, color);

        int channels = returnImage.channels();

        for (int row = 0; row < height; row++) {
            for (int col = 0; col < width; col++) {
                double[] data = returnImage.get(row, col);

                double newValue = transformationTypeFunctionMap.get(testType).apply(data);
                for (int ch = 0; ch < channels; ch++) {
                    data[ch] = newValue;
                }
                returnImage.put(row, col, data);
            }
        }
        return returnImage;
    }

    private Mat getFloydSteinbergDitheringImage(Mat image) {
        Mat output = image.clone();
        int height = image.rows();
        int width = image.cols();
        int channels = image.channels();

        double threshold = Math.floor(255 / 2), pixel, value;
        int error;

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                pixel = image.get(y, x)[0];
                output.put(y, x, pixel < threshold ? 0 : 255);

                error = (int) pixel - (int) output.get(y, x)[0];
                if (x + 1 < width) {
                    value = image.get(y, x + 1)[0] + error * 7 / 16;
                    image.put(y, x + 1, value);
                }
                if (x + 1 < width && y + 1 < height) {
                    value = image.get(y + 1, x + 1)[0] + error * 1 / 16;
                    image.put(y + 1, x + 1, value);
                }
                if (y + 1 < height) {
                    value = image.get(y + 1, x - 1)[0] + error * 3 / 16;
                    image.put(y + 1, x - 1, value);

                    value = image.get(y + 1, x)[0] + error * 5 / 16;
                    image.put(y + 1, x, value);
                }
            }
        }
        return image;
    }

    @Ignore
    private Mat colorizeImage(Mat image) {
        Mat empty = Mat.zeros(image.size(), CvType.CV_8UC1);
        List<Mat> channels = Arrays.asList(image, empty, empty);
        Mat color = new Mat();
        Core.merge(channels, color);
        return color;
    }

    @Ignore
    private Mat colorizeImage(Mat image, int colorMap) {
        Mat output = image.clone();
        Imgproc.applyColorMap(image, output, colorMap);

        return output;
    }

    public void solve() {
        String IMAGE_NAME = "face.jpg";
        Mat image = Imgcodecs.imread(Main.IMPORT_DIRECTORY + IMAGE_NAME);
        System.out.println(image.rows() + " " + image.cols());

        Arrays.stream(TransformationType.values()).forEach((testType -> {
            Mat resultImage = imageToGrayscale(image, testType, testType.getColor());
            Imgcodecs.imwrite(Main.getFileNameWithSuffix(Main.HOMEWORK_TWO_DIRECTORY, IMAGE_NAME, testType.toString()), resultImage);
        }));

        Mat auxImage = image.clone();
        Imgproc.cvtColor(image, auxImage, Imgproc.COLOR_BGR2GRAY, 1);

        Mat floydSteinbergDitheringImage = getFloydSteinbergDitheringImage(auxImage);
        Imgcodecs.imwrite(Main.getFileNameWithSuffix(Main.HOMEWORK_TWO_DIRECTORY, IMAGE_NAME, "Dithering."), floydSteinbergDitheringImage);

        for (int i = 0; i < 13; i++) {
            auxImage = image.clone();
            Imgproc.cvtColor(image, auxImage, Imgproc.COLOR_BGR2GRAY);
            Mat colorizedImage = colorizeImage(auxImage, i);
            Imgcodecs.imwrite(Main.HOMEWORK_TWO_DIRECTORY + IMAGE_NAME.replace(".", "_Colorized_" + i + "."), colorizedImage);
            Imgcodecs.imwrite(Main.getFileNameWithSuffix(Main.HOMEWORK_TWO_DIRECTORY, IMAGE_NAME, "Colorized_" + i), colorizedImage);
        }
    }

}