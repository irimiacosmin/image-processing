public class Main {

    public static final String IMPORT_DIRECTORY = "data/";
    public static final String TESSDATA_DIRECTORY = "Tess4J/tessdata";

    public static final String EXPORT_DIRECTORY = "results/";
    public static final String HOMEWORK_ONE_DIRECTORY = EXPORT_DIRECTORY + "h1/";
    public static final String HOMEWORK_TWO_DIRECTORY = EXPORT_DIRECTORY + "h2/";
    public static final String HOMEWORK_THREE_DIRECTORY = EXPORT_DIRECTORY + "h3/";

    public static String getFileNameWithSuffix(String directory, String filename, String suffix) {
        return directory + filename.replace(".", "_" + suffix + ".");
    }

    public static String getFileNameWithSuffix(String filename, String suffix) {
        return filename.replace(".", "_" + suffix + ".");
    }

    public static void main(String[] args) {
        nu.pattern.OpenCV.loadShared();
//        new HomeworkOne().solve();
//        new HomeworkTwo().solve();
        new HomeworkThree().solve();
    }
}
