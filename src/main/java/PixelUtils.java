public class PixelUtils {

    public static double max(double r, double g, double b) {
        return Math.max(r, Math.max(g, b));
    }

    public static double min(double r, double g, double b) {
        return Math.min(r, Math.min(g, b));
    }

}
