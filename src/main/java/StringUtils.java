import info.debatty.java.stringsimilarity.NormalizedLevenshtein;

public class StringUtils {

    private static final NormalizedLevenshtein normalizedLevenshtein = new NormalizedLevenshtein();

    public static Double compare(String s1, String s2) {
        return 1 - normalizedLevenshtein.distance(s1, s2);
    }
}