package homeworkThree;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;

import java.util.Random;
import java.util.function.Function;

import static homeworkThree.Limitation.BlurType.*;

@Getter
@AllArgsConstructor
public enum Limitation {
    NONE(image -> image),
    GAUSSIAN_NOISE(image -> {
        Mat noise = Mat.zeros(image.size(), image.type());
        MatOfDouble mean = new MatOfDouble();
        MatOfDouble dev = new MatOfDouble();
        Core.meanStdDev(image, mean, dev);
        Core.randn(noise, mean.get(0, 0)[0], dev.get(0, 0)[0]);
        Core.add(image, noise, image);
        return image;
    }),
    SALT_LOW_PAPER_NOISE(image -> applySaltPaperNoise(image, 0.005)),
    SALT_MEDIUM_PAPER_NOISE(image -> applySaltPaperNoise(image, 0.05)),
    SALT_HIGH_PAPER_NOISE(image -> applySaltPaperNoise(image, 0.10)),
    ROTATION_90(image -> rotate(image, Core.ROTATE_90_CLOCKWISE)),
    ROTATION_180(image -> rotate(image, Core.ROTATE_180)),
    SHEAR_VERTICAL(image -> commonShear(image, 2)),
    SHEAR_HORIZONTAL(image -> commonShear(image, 1)),
    SIZE_HALF(image -> crop(image, 0.50)),
    SIZE_QUARTER(image -> crop(image, 0.25)),
    SIZE_DOUBLE(image -> crop(image, 2)),
    GAUSSIAN_BLUR(image -> blur(image, GAUSSIAN)),
    BOX_BLUR(image -> blur(image, BOX)),
    MEDIAN_BLUR(image -> blur(image, MEDIAN)),
    BILATERAL_BLUR(image -> blur(image, BILATERAL));

    private static Mat commonShear(Mat image, int type) {
        int rows = image.height(), columns = image.width();
        int fifthOfRows = rows / 5, fifthOfColumns = columns / 5;
        Mat output = image.clone();
        Mat shearedMatrix = shearOfType(image, rows, columns, fifthOfRows, fifthOfColumns, type);
        Imgproc.warpAffine(image, output, shearedMatrix, new Size(4 * (rows + 2 * (fifthOfRows)), columns + fifthOfColumns));
        return output;
    }
    
    private static Mat shearOfType(Mat image, int rows, int columns, int fifthOfRows, int fifthOfColumns, int shearType) {
        MatOfPoint2f startingPoints, endingPoints;
        if (shearType == 1) {
            startingPoints = new MatOfPoint2f(
                    new Point(0, 0),
                    new Point(rows - fifthOfRows, fifthOfColumns),
                    new Point(fifthOfRows, columns - fifthOfColumns));
            endingPoints = new MatOfPoint2f(
                    new Point(0, 0),
                    new Point(rows - fifthOfRows, fifthOfColumns),
                    new Point((rows - fifthOfRows), columns - fifthOfColumns));
            Core.copyMakeBorder(image, image, fifthOfRows, fifthOfRows, 3 * fifthOfRows, fifthOfRows, Core.BORDER_CONSTANT, new Scalar(0, 0, 0));
            return Imgproc.getAffineTransform(endingPoints, startingPoints);
        } else if (shearType == 2) {
            startingPoints = new MatOfPoint2f(
                    new Point(fifthOfRows, fifthOfColumns),
                    new Point(rows - fifthOfRows, fifthOfColumns),
                    new Point(fifthOfRows, columns - fifthOfColumns));
            endingPoints = new MatOfPoint2f(
                    new Point((double) (rows - fifthOfRows) / 20, (double) (columns - fifthOfColumns) / 2),
                    new Point(rows - fifthOfRows, fifthOfColumns),
                    new Point((double) (rows - fifthOfRows) / 2, columns - fifthOfColumns));
            Core.copyMakeBorder(image, image, fifthOfRows, fifthOfRows, fifthOfRows, fifthOfRows, Core.BORDER_CONSTANT, new Scalar(0, 0, 0));
            return Imgproc.getAffineTransform(endingPoints, startingPoints);
        }
        return image.clone();
    }

    private static Mat applySaltPaperNoise(Mat image, double prob) {
        Mat output = Mat.zeros(image.size(), CvType.CV_8UC1);
        double threshold = 1 - prob;
        for (int row = 0; row < image.height(); row++) {
            for (int col = 0; col < image.width(); col++) {
                double rand = random.nextDouble();
                if (rand < prob) {
                    output.put(row, col, 0);
                } else if (rand > threshold) {
                    output.put(row, col, 255);
                } else {
                    output.put(row, col, image.get(row, col));
                }
            }
        }
        return output;
    }

    private static Mat rotate(Mat image, int rotateType) {
        Mat output = image.clone();
        Core.rotate(image, output, rotateType);
        return output;
    }

    private static Mat crop(Mat image, double percent) {
        Mat output = image.clone();
        Imgproc.resize(image, output, new Size(image.cols() * percent, image.rows() * percent), 0, 0, Imgproc.INTER_AREA);
        return output;
    }

    private static Mat blur(Mat image, BlurType type) {
        Mat output = image.clone();
        int radius = (int) ((double) 2 + 0.5);
        int kernelSize;
        switch (type) {
            case BOX:
                kernelSize = 2 * radius + 1;
                Imgproc.blur(image, output, new Size(kernelSize, kernelSize));
                break;
            case GAUSSIAN:
                kernelSize = 6 * radius + 1;
                Imgproc.GaussianBlur(image, output, new Size(kernelSize, kernelSize), radius);
                break;
            case MEDIAN:
                kernelSize = 2 * radius + 1;
                Imgproc.medianBlur(image, output, kernelSize);
                break;
            case BILATERAL:
                Imgproc.bilateralFilter(image, output, -1, radius, radius);
                break;
        }
        return output;
    }

    private static final Random random = new Random();

    private final Function<Mat, Mat> method;

    public enum BlurType {
        BOX, GAUSSIAN, MEDIAN, BILATERAL
    }
}
