package homeworkThree;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum OCRFile {
//    DIP_GW_1("dip_gw_1.jpg"),
//    LAB_3_DIP_PAG_1("Lab_3_DIP.tiff"),
//    LAB_3_DIP_PAG_2("Lab_3_DIP_Page_2.tiff"),
//    LAB_3_DIP_PAG_3("Lab_3_DIP_Page_3.tiff"),
//    LAB_3_DIP_PAG_4("Lab_3_DIP_Page_4.tiff"),
//    LAB_3_DIP_PAG_5("Lab_3_DIP_Page_5.tiff"),
    SAMPLE("sample.PNG"),
//    SCANNED_IMAGE_1("scanned_image_1.png"),
//    SCANNED_IMAGE_2("scanned_image_2.png"),
//    SCANNED_IMAGE_3("scanned_image_3.jpg"),
//    CAPTURE("CAPTURE.PNG"),
//    GROUP("group.jpg")
    ;



    private final String fileName;
}