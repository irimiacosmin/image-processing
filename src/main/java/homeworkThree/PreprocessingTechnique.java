package homeworkThree;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.function.Function;

@Getter
@AllArgsConstructor
public enum PreprocessingTechnique {
    NONE(image -> image),
    LETTERS(image -> getTechnique(image, 1)),
    CONTOUR(image -> getTechnique(image, 2)),
    MORPHOLOGICAL(image -> getTechnique(image, 3)),
    THRESHOLD(image -> getTechnique(image, 4)),
    SHARPEN(image -> {
        Mat kernel = Mat.ones(new Size(3, 3), CvType.CV_32F);
        for (int row = 0; row < kernel.height(); row++) {
            for (int col = 0; col < kernel.width(); col++) {
               kernel.put(row, col, -1);
            }
        }
        kernel.put(1, 1, 9);
        Mat output = image.clone();
        Imgproc.filter2D(image, output, -1, kernel);

        return output;
    });

    private static Mat getTechnique(Mat image, int pt) {
        Mat threshold = image.clone();
        Imgproc.threshold(image, threshold, 0, 255, Imgproc.THRESH_BINARY);
        Mat kernel = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(5, 5));
        Mat morph = image.clone();
        Imgproc.morphologyEx(threshold, morph, Imgproc.MORPH_OPEN, kernel);
        Mat letter = morph.clone();
        Mat edges = new Mat();
        Imgproc.Canny(letter, edges, 200, 200);
        switch (pt) {
            case 1: {
                return letter;
            }
            case 2: {
                return edges;
            }
            case 3: {
                return morph;
            }
            case 4: {
                return threshold;
            }
            default: {
                return image;
            }
        }
    }

    private final Function<Mat, Mat> method;
}